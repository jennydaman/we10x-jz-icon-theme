#!/bin/bash

from_mcmuse=(
  src/scalable/apps/cheese.svg           # org.gnome.Cheese
  src/scalable/apps/evolution-tasks.svg  # org.gnome.GPaste.Ui
  src/scalable/apps/tomboy.svg           # org.gnome.Notes
)

for icon in ${from_mcmuse[*]}; do
  cp -v {McMuse,We10X}-icon-theme/$icon
done

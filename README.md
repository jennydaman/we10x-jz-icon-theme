Fix missing icons in *We10X-icon-theme*

```bash
# 1. fetch sources
git submodule update --init --recursive
# 2. apply patches
./patch.sh
# 3. install theme
cd We10X-icon-theme && ./install.sh -green
```

# List of Changes

- org.gnome.GPaste.Ui
- org.gnome.Cheese
- org.gnome.Notes

